# RollenspielMonster-Website

[![Codeberg CI](https://ci.codeberg.org/api/badges/RollenspielMonster/website/status.svg)](https://ci.codeberg.org/RollenspielMonster/website)
![Framework](https://img.shields.io/badge/Framework-hugo-pink)
![Lizenz](https://img.shields.io/badge/Lizenz-CC--BY--NC--SA--4.0-orange)

Dieses Repository enthält den Quellcode für die RollenspielMonster-Website. Die Website dient als Präsentationsplattform für RollenspielMonster, einem Anbieter von verschiedenen Softwarelösungen als Alternative zu GAFAM (Google, Apple, Facebook, Amazon, Microsoft).

## Inhaltsverzeichnis

- [RollenspielMonster-Website](#rollenspielmonster-website)
  - [Inhaltsverzeichnis](#inhaltsverzeichnis)
  - [Über die RollenspielMonster-Website](#über-die-rollenspielmonster-website)
  - [Installation](#installation)
  - [Verwendung](#verwendung)
  - [Beitragende](#beitragende)
  - [Kontakt](#kontakt)
  - [Lizenz](#lizenz)

## Über die RollenspielMonster-Website

Die RollenspielMonster-Website stellt Informationen über RollenspielMonster bereit, ein Anbieter von verschiedenen Softwarelösungen als Alternative zu den großen Tech-Giganten. RollenspielMonster bietet eine Vielzahl von Diensten und Tools an, die dazu dienen, die Abhängigkeit von GAFAM zu reduzieren und eine datenschutzorientierte und ethische Alternative zu bieten.

## Installation

Um die RollenspielMonster-Website lokal auszuführen, führen Sie bitte die folgenden Schritte aus:

1. Stellen Sie sicher, dass Sie eine aktuelle Version von Hugo auf Ihrem System installiert haben.

2. Klonen Sie das Repository mit Git:
```
git clone https://codeberg.org/RollenspielMonster/website.git
```

1. Wechseln Sie in das Verzeichnis der Website:
```
cd website
```

1. Führen Sie den Befehl `hugo server` aus, um den Entwicklungsserver zu starten.

## Verwendung

Nachdem Sie den Entwicklungsserver gestartet haben, können Sie die RollenspielMonster-Website in Ihrem Webbrowser unter der Adresse http://localhost:1313 anzeigen. Der Entwicklungsserver überwacht Änderungen an den Dateien und lädt die Website automatisch neu, wenn Sie Änderungen vornehmen.

## Beitragende

Beiträge zur RollenspielMonster-Website sind willkommen! Wenn Sie Verbesserungen vornehmen möchten, führen Sie bitte die folgenden Schritte aus:

1. Forken Sie das Repository.
2. Erstellen Sie einen neuen Branch für Ihre Änderungen.
3. Führen Sie Ihre Änderungen durch und veröffentlichen Sie diese in Ihrem Fork.
4. Erstellen Sie einen Pull-Request, um Ihre Änderungen in das Hauptrepository einzubringen.

Wir schätzen Ihre Beiträge und werden sie überprüfen und zusammenführen, um die RollenspielMonster-Website kontinuierlich zu verbessern.

## Kontakt

Bei Fragen oder Anliegen können Sie uns über das Kontaktformular auf der [Kontaktseite](https://rollenspiel.monster/contact/) der RollenspielMonster-Website erreichen.

## Lizenz

Die RollenspielMonster-Website ist unter der [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](LICENSE) lizenziert. Weitere Informationen finden Sie in der Lizenzdatei.
