---
title: Lemmy
description: Erweiterungen für den Dienst Lemmy
date: 2023-11-06T13:56:04.467Z
lastmod: 2023-11-06T18:44:04.467Z
tags:
  - ""
categories:
  - ""
slug: lemmy
showParent: true
---

## Lemmy

### Kennzeichnungspflicht

Folgende Inhalte werden entfernt, wenn sie ohne NSFW Kennzeichnung auftauchen:

- Werbung
- Pornografische und/oder sexueller Inhalte
- Gewaltdarstellungen in Wort und Bild
- Religiöse & politische Inhalte

Wir werden auch alle Inhalte entfernen, die wir für hasserfüllt gegenüber bestimmten Einzelpersonen oder Gruppen halten. Inhalte, die dazu bestimmt sind, Schaden zu verursachen oder anzuregen. Diese Richtlinie ist absichtlich vage formuliert, weil wir nicht beabsichtigen "Rechtsanwalt" zu spielen, um zu sagen, was in Ordnung ist und was nicht. Eine gute Faustregel ist:

> Wenn du etwas sagen willst, von dem du denkst, dass es mit dieser Regel nicht übereinstimmt, solltest du es wahrscheinlich nicht sagen.

### Automatisierte Beiträge

- Wenn du überwiegend automatische Beiträge veröffentlichst, kennzeichne dein Account bitte als Bot.

### Föderationsstatus

rollenspiel.forum glaubt nicht an Zensur. Fediverse ist ein Netzwerkdienst und die Aussetzung der Föderation mit einem anderen Server wird nur als letztes Mittel durchgeführt, wenn Inhalte von einem anderen Server nach deutschem Recht Probleme verursachen können, oder bei technischen Schwierigkeiten, feindlichen oder bösartigen Aktivitäten. Wenn wir aus einem, oder mehreren dieser Gründe die Föderation aussetzen oder eine andere Instanz zum Schweigen bringen müssen, wird dies unter "Blockierte Instanzen" vermerkt. Haben wir deiner Meinung nach eine falsche Entscheidung getroffen? [Kontaktiere uns](https://rollenspiel.monster/contact)!
