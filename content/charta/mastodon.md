---
title: Mastodon
description: Erweiterungen für den Dienst Mastodon
date: 2023-01-29T15:48:33.000Z
lastmod: 2023-11-06T18:44:04.467Z
tags:
  - ""
categories:
  - ""
slug: mastodon
showParent: true
---

## Mastodon

### [Kennzeichnung](https://rollenspiel.wiki/books/mastodon-ap/page/inhaltswarnung-cw)spflicht

Folgende Inhalte werden entfernt, wenn sie in der öffentlichen Zeitleiste ohne [Kennzeichnung](https://rollenspiel.wiki/books/mastodon-ap/page/inhaltswarnung-cw) auftauchen:

- Werbung
- Pornografische und/oder sexueller Inhalte
- Gewaltdarstellungen in Wort und Bild
- Religiöse & politische Inhalte
- Crossposting

Wir werden auch alle Inhalte aus der öffentlichen Zeitleiste entfernen, die wir für hasserfüllt gegenüber bestimmten Einzelpersonen oder Gruppen halten. Inhalte, die dazu bestimmt sind, Schaden zu verursachen oder anzuregen. Diese Richtlinie ist absichtlich vage formuliert, weil wir nicht beabsichtigen "Rechtsanwalt" zu spielen, um zu sagen, was in Ordnung ist und was nicht. Eine gute Faustregel ist:

> Wenn du etwas sagen willst, von dem du denkst, dass es mit dieser Regel nicht übereinstimmt, solltest du es wahrscheinlich nicht sagen.

### Threads

Wenn ihr mehrere inhaltlich zusammen hängenden Beiträge hintereinander veröffentlicht bzw. [Threads](https://rollenspiel.wiki/books/mastodon-ap/page/antworten-und-threads#bkmrk-threads) erstellt, sind diese als "[Nicht gelistet](https://rollenspiel.wiki/books/mastodon-ap/page/sichtbarkeit-und-zielgruppen#bkmrk-nicht-gelistet)" zu erstellen.

### Automatisierte Beiträge

- Wenn du überwiegend automatische Beiträge veröffentlichst, kennzeichne dein Account bitte als Bot.
- Wenn mehr als fünf Beiträge am Tag automatisiert veröffentlicht werden, sind diese Beiträge als "Nicht gelistet"/"unlisted" zu erstellen.

Crossposting von unfreier Software nach Mastodon ist nur als "Nicht gelistet"/"unlisted" gestattet; noch besser wäre ihr crosspostet von Mastodon aus.

### Föderationsstatus

rollenspiel.social glaubt nicht an Zensur. Fediverse ist ein Netzwerkdienst und die Aussetzung der Föderation mit einem anderen Server wird nur als letztes Mittel durchgeführt, wenn Inhalte von einem anderen Server nach deutschem Recht Probleme verursachen können, bei technischen Schwierigkeiten, feindlichen oder bösartigen Aktivitäten. Wenn wir aus einem, oder mehreren dieser Gründe die Föderation aussetzen oder eine andere Instanz zum Schweigen bringen müssen, wird dies unter "Moderierte Server" vermerkt. Haben wir deiner Meinung nach eine falsche Entscheidung getroffen? [Kontaktiere uns](https://rollenspiel.monster/contact)!
