---
title: Matrix
description: Erweiterungen für den Dienst Matrix bzw die Clients Element und Cinny
date: 2023-01-29T15:48:33.000Z
lastmod: 2023-11-06T18:44:04.467Z
tags:
  - ""
categories:
  - ""
slug: matrix
showParent: true
---

## Matrix

### Ressourcen

Matrix ist nicht besonders effizient und benötigt leistungsstarke Hardware. Dieser Dienst wird von einer Privatperson betrieben, die dafür zu zahlen hat.

**Zu vermeiden ist:**

- Erstellen von Räumen, die zu großen Telegram-Kanälen mit tausenden von Benutzer:innen verbunden wurden, die dann sinnlos auf Matrix gespiegelt werden.
- Das gilt auch für Discord.
- Verbinden von großen IRC Räumen, die vielleicht sogar schon existieren oder anderweitig bereits verbunden sind.
- Das gilt natürlich auch für Telegram und Discord!
- Den Dienst als Feedreader für populäre Twitter-Hashtags nutzen.
- Den Dienst als Feedreader für populäre Blogs (wie Tumblr) nutzen.
- Mit deinen Freunden tausende von Shitposting-Memes auf den Dienst abladen.

### Massen-Einladungen

Solange die Einladungen nicht von einem unserer internen Server-Moderatoren gestattet wurden; die Einwilligung der einzuladenden User nicht vorliegt; oder du nicht für eine Organisation handelst, die ihre legitimen und identifizierbaren Mitglieder orchestrieren muss, sind Masseninvites verboten.

### Inhalte melden

Zwar gibt es in vielen Clients ein Melden-Feature, jedoch ist das zur Moderation noch schlecht implementiert, und dein Admin wird nicht automatisch benachrichtigt. Am besten benutzt du das Feature trotzdem, um unter anderem die ID der Nachricht(en) zu übermitteln, sagst aber zusätzlich via [Support](https://ticket.rollenspiel.monster) oder Matrix Bescheid. Gerne mit Screenshots.
