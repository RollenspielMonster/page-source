---
title: Imprint
date: 2020-09-01T00:00:00.000Z
lastmod: 2023-02-03T18:51:40.052Z
tags:
  - ""
categories:
  - ""
slug: imprint
---

## Service provider

<p class="bot">
kcuB leinaD<br/>
<span style="font-size: 10px;">neinareM nov gozreH</span><br/>
2 .rtsnehcriK<br/>
nralsE 39629
</p>

## Contact options

**E-mail address:** <spawn class="bot">retsnom.leipsnellor@retsambew</spawn> <a href="https://rollenspiel.monster/.well-known/openpgpkey/hu/kd39y8fkyw5j8uubuicshffo9hhodk4j?l=webmaster"><i class="fa fa-gnupg" aria-hidden="true"></i>-Key</a><br/>
**Phone:** <spawn class="bot">77736587 651 94+</spawn>

## Social media and other online presences

This imprint also applies to the following domains and their domains:

- https://rollenspiel.chat
- https://rollenspiel.cloud
- https://rollenspiel.events
- https://rollenspiel.group
- https://rollenspiel.network
- https://rollenspiel.social
- https://rollenspiel.wiki

## Liability and property rights information

**Disclaimer**: The contents of this online offer have been prepared carefully and according to our current knowledge, but are for information purposes only and do not have any legally binding effect, unless it is legally obligatory information (e.g. the imprint, privacy policy, terms and conditions or obligatory instructions of consumers). We reserve the right to change or delete the contents in whole or in part, insofar as contractual obligations remain unaffected. All offers are subject to change and non-binding.

**Links to external websites**: Contents of external websites to which we refer directly or indirectly are outside our area of responsibility and we do not adopt them as our own. The provider of the linked websites is solely liable for all contents and in particular for damages resulting from the use of the information available on the linked websites.

**Copyrights and trademark rights**: All content displayed on this website, such as texts, photographs, graphics, brands and trademarks are protected by the respective protective rights (copyrights, trademark rights). The use, reproduction, etc. are subject to our rights or the rights of the respective authors or rights managers.

**Notices of legal violations**: If you notice any legal violations within our Internet presence, we ask you to point them out to us. We will remove illegal content and links immediately after becoming aware of them.

## Address processing

All the contact information of me given on this website including any photos are expressly for information purposes only or to contact me. In particular, they may not be used for sending advertising, spam and the like. An advertising use of this data is therefore hereby contradicted. Should this information nevertheless be used for the aforementioned purposes, I reserve the right to take legal action.

[Erstellt mit kostenlosem Datenschutz-Generator.de von Dr. Thomas Schwenke](https://datenschutz-generator.de/?l=de "Rechtstext von Dr. Schwenke - für weitere Informationen bitte anklicken.")
