---
title: Impressum
date: 2020-09-01T00:00:00.000Z
lastmod: 2023-02-03T18:51:40.052Z
tags:
  - ""
categories:
  - ""
slug: imprint
---

## Diensteanbieter

<p class="bot">
kcuB leinaD<br/>
<span style="font-size: 10px;">neinareM nov gozreH</span><br/>
2 .rtsnehcriK<br/>
nralsE 39629
</p>

## Kontaktmöglichkeiten

**E-Mail-Adresse:** <spawn class="bot">retsnom.leipsnellor@retsambew</spawn> <a href="https://rollenspiel.monster/.well-known/openpgpkey/hu/kd39y8fkyw5j8uubuicshffo9hhodk4j?l=webmaster"><i class="fa fa-gnupg" aria-hidden="true"></i>-Schlüssel</a><br/>
**Telefon:** <spawn class="bot">77736587 651 94+</spawn>

## Social Media und andere Onlinepräsenzen

Dieses Impressum gilt auch folgende Domains und deren Subdomains:

- https://rollenspiel.chat
- https://rollenspiel.cloud
- https://rollenspiel.events
- https://rollenspiel.group
- https://rollenspiel.network
- https://rollenspiel.social
- https://rollenspiel.wiki

## Haftungs- und Schutzrechtshinweise

**Haftungsausschluss**: Die Inhalte dieses Onlineangebotes wurden sorgfältig und nach unserem aktuellen Kenntnisstand erstellt, dienen jedoch nur der Information und entfalten keine rechtlich bindende Wirkung, sofern es sich nicht um gesetzlich verpflichtende Informationen (z. B. das Impressum, die Datenschutzerklärung, AGB oder verpflichtende Belehrungen von Verbrauchern) handelt. Wir behalten uns vor, die Inhalte vollständig oder teilweise zu ändern oder zu löschen, soweit vertragliche Verpflichtungen unberührt bleiben. Alle Angebote sind freibleibend und unverbindlich.

**Links auf fremde Webseiten**: Inhalte fremder Webseiten, auf die wir direkt oder indirekt verweisen, liegen außerhalb unseres Verantwortungsbereiches und machen wir uns nicht zu eigen. Für alle Inhalte und insbesondere für Schäden, die aus der Nutzung der in den verlinkten Webseiten aufrufbaren Informationen entstehen, haftet allein der Anbieter der verlinkten Webseiten.

**Urheberrechte und Markenrechte**: Alle auf dieser Website dargestellten Inhalte, wie Texte, Fotografien, Grafiken, Marken und Warenzeichen sind durch die jeweiligen Schutzrechte (Urheberrechte, Markenrechte) geschützt. Die Verwendung, Vervielfältigung usw. unterliegen unseren Rechten oder den Rechten der jeweiligen Urheber bzw. Rechteverwalter.

**Hinweise auf Rechtsverstöße**: Sollten Sie innerhalb unseres Internetauftritts Rechtsverstöße bemerken, bitten wir Sie, uns auf diese hinzuweisen. Wir werden rechtswidrige Inhalte und Links nach Kenntnisnahme unverzüglich entfernen.

## Adressverarbeitung

Alle die auf dieser Webseite angegebenen Kontaktinformationen von mir inklusive etwaiger Fotos dienen ausdrücklich nur zu Informationszwecken bzw. zur Kontaktaufnahme. Sie dürfen insbesondere nicht für die Zusendung von Werbung, Spam und ähnliches genutzt werden. Einer werblichen Nutzung dieser Daten wird deshalb hiermit widersprochen. Sollten diese Informationen dennoch zu den vorstehend genannten Zwecken genutzt werden, behalte ich mir etwaige rechtliche Schritte vor.

[Erstellt mit kostenlosem Datenschutz-Generator.de von Dr. Thomas Schwenke](https://datenschutz-generator.de/?l=de "Rechtstext von Dr. Schwenke - für weitere Informationen bitte anklicken.")
