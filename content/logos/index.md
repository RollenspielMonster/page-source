---
title: Unsere Logos
date: 2020-09-01T00:00:00.000Z
lastmod: 2021-05-31T16:58:39.388Z
tags:
  - ""
categories:
  - ""
slug: logos
---

## Auflistung unserer Logo Varianten


### Maskottchen mit Hintergrund

<img loading="lazy" src="615x593_cut.png" alt="615x593" style="max-width: 360px;">

### Maskottchen ohne Hintergrund

<img loading="lazy" src="887x739.png" alt="887x739" style="max-width: 360px;">

<img loading="lazy" src="912x760.png" alt="912x760" style="max-width: 360px;">

### Logo Schrift rechts einzeilig

<img loading="lazy" src="365x100.png" alt="365x100" style="max-width: 360px;">

### Logo Schrift rechts zweizeilig

<img loading="lazy" src="1153x446.png" alt="1153x446" style="max-width: 360px;">

### Logo Schrift unten einzeilig

<img loading="lazy" src="1125x762.png" alt="1125x762" style="max-width: 360px;">