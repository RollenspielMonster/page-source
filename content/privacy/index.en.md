---
title: Privacy policy
date: 2020-09-01T00:00:00.000Z
lastmod: 2023-02-03T18:51:44.891Z
tags:
  - ""
categories:
  - ""
slug: privacy
---

## Introduction

We have consciously decided against the use of tracking cookies, analysis tools or other direct integration of external content. Operators of linked websites only get your information when you click on the link. This procedure is not according to the "mainstream" and has disadvantages for the ranking at Google and Co., but we gladly accept this. We handle this way to promote [informational self-determination](https://de.wikipedia.org/wiki/Informationelle_Selbstbestimmung), or more precisely, not to unnecessarily endanger your IT security.

The terms used are not gender specific.

## Name and contact details of the controller

<p class="bot">
kcuB leinaD<br/>
<span style="font-size: 10px;">neinareM nov gozreH</span><br/>
2 .rtsnehcriK<br/>
nralsE 39629
</p>

**E-mail address:** <spawn class="bot">retsnom.leipsnellor@retsambew</spawn> <a href="https://rollenspiel.monster/.well-known/openpgpkey/hu/kd39y8fkyw5j8uubuicshffo9hhodk4j?l=webmaster"><i class="fa fa-gnupg" aria-hidden="true"></i>-Key</a><br/>
**Phone:** <spawn class="bot">77736587 651 94+</spawn>

## Contact for data protection issues

Bei Fragen zum Datenschutz stehen wir unter <a class="bot">retsnom.leipsnellor@ztuhcsnetad</a> <a href="https://rollenspiel.monster/.well-known/openpgpkey/hu/i7jib9hs56n983nznn7ohu6yz49drzkb?l=datenschutz"><i class="fa fa-gnupg" aria-hidden="true"></i>-Schlüssel</a> oder unter der oben angegebenen postalischen Anschrift zur Verfügung.

## Provision of the online offer and web hosting

### Hosting

We host the content of our website with the following provider:

#### Hetzner

The provider is Hetzner Online GmbH, Industriestr. 25, 91710 Gunzenhausen (hereinafter referred to as Hetzner).

For details, please refer to Hetzner's privacy policy: https://www.hetzner.com/de/rechtliches/datenschutz.

The use of Hetzner is based on Art. 6 para. 1 lit. f DSGVO. We have a legitimate interest in the most reliable presentation of our website. Insofar as a corresponding consent has been requested, the processing is carried out exclusively on the basis of Art.
6 para. 1 lit. a DSGVO and § 25 para. 1 TTDSG, insofar as the consent includes the storage of cookies or access to information in the user's terminal device (e.g. device fingerprinting) as defined by the TTDSG. The consent can be revoked at any time.

##### Job processing

We have concluded a contract on order processing (AVV) for the use of the above-mentioned service. This is a contract required by data protection law, which ensures that this processes the personal data of our website visitors only according to our instructions and in compliance with the GDPR.

### Collection of access data and log files

When you visit our website, your browser systematically transmits certain information to the server, which in turn could make you identifiable. For example, you, or more precisely your web browser, transmit information such as the IP address assigned by your provider.

You cannot prevent this system-related behavior of your browser, and therefore you have no choice but to trust that we handle the transmitted information sensitively. Because - even if it may sound a little trite - the protection of your data is actually close to our hearts, we will handle the transmitted information in the best possible way to represent your interests. Therefore, we do not collect, process or use the submitted information for analysis, advertising or similar purposes. Rather, we do not store any information about you - the web server based on nginx is configured so that it does not write log files:

```
## LOGS ##
access_log off;
error_log off;
```

### SSL or TLS encryption

This site uses SSL or TLS encryption for security reasons and to protect the transmission of confidential content, such as orders or requests that you send to us as the site operator. You can recognize an encrypted connection by the fact that the address line of the browser changes from "http://" to "https://" and by the lock symbol in your browser line.

If SSL or TLS encryption is activated, the data you send to us cannot be read by third parties.

### Cookies use

Cookies are text files that contain data from visited websites or domains and are stored by a browser on the user's computer. A cookie is primarily used to store information about a user during or after his visit within an online offer. Stored information may include, for example, language settings on a website, login status, a shopping cart, or where a video was watched. The term cookies also includes other technologies that perform the same functions as cookies (e.g., when user information is stored using pseudonymous online identifiers, also known as "user IDs").

The data processed by the cookies are necessary for the provision of our services and thus to protect our legitimate interests as well as those of third parties pursuant to Art. 6 (1) p. 1 lit. f DSGVO. The cookies set, which are necessary, for example, for logging in, are persistent cookies, which, however, must not be confused with third-party tracking cookies that track a user across pages on the Internet.

**Note**: Most browsers accept cookies automatically. However, you can configure your browser so that no cookies are stored on your computer or a notice always appears before a new cookie is created. However, completely disabling cookies may prevent you from using all the features of our website.

### Plugins and tools

#### hCaptcha

We use hCaptcha (hereinafter "hCaptcha") on this website. The provider is Intuition Machines, Inc, 2211 Selig Drive, Los Angeles, CA 90026, USA (hereinafter referred to as "IMI").

The purpose of hCaptcha is to check whether data is entered on this website (e.g. in a contact form) by a human or by an automated program. For this purpose, hCaptcha analyzes the behavior of the website visitor based on various characteristics.

This analysis begins automatically as soon as the website visitor enters a website with activated hCaptcha. For the analysis, hCaptcha evaluates various information (e.g. IP address, time spent on the website by the website visitor or mouse movements made by the user). The data collected during the analysis is forwarded to IMI. If hCaptcha is used in "invisible mode", the analyses run completely in the background. Website visitors are not informed that an analysis is taking place.

The data is stored and analyzed on the basis of Art. 6 para. 1 lit. f GDPR. The website operator has a legitimate interest in protecting its website from abusive automated spying and SPAM. If a corresponding consent has been requested, the processing is carried out exclusively on the basis of Art. 6 para. 1 lit. a GDPR and § 25 para. 1 TTDSG, insofar as the consent includes the storage of cookies or access to information in the user's terminal device (e.g. device fingerprinting) within the meaning of the TTDSG. Consent can be revoked at any time.

Data processing is based on standard contractual clauses contained in the data processing addendum to IMI's General Terms and Conditions or the data processing contracts.

Further information on hCaptcha can be found in the privacy policy and terms of use at the following links: [https://www.hcaptcha.com/privacy](https://www.hcaptcha.com/privacy) and [https://hcaptcha.com/terms](https://hcaptcha.com/terms).

The company is certified in accordance with the "EU-US Data Privacy Framework" (DPF). The DPF is an agreement between the European Union and the USA, which is intended to ensure compliance with European data protection standards for data processing in the USA. Every company certified under the DPF undertakes to comply with these data protection standards. Further information on this can be obtained from the provider at the following link: [https://www.dataprivacyframework.gov/list](https://www.dataprivacyframework.gov/list)

## Contact

When contacting us (e.g. via contact form, e-mail, telephone or via social media), the information of the inquiring persons will be processed as far as this is necessary to answer the contact requests and any requested measures. In this context, the provision of a valid e-mail address is required so that I know from whom the request originates and in order to be able to answer it. Further information can be provided voluntarily.

## Presence in social networks (social media)

We maintain online presences within social networks and process user data in this context in order to communicate with users active there or to offer information about us.

We would like to point out that user data may be processed outside the European Union. This may result in risks for users, for example because it could make it more difficult to enforce users' rights.

## Data deletion

The data processed by us will be deleted in accordance with the legal requirements as soon as their consents permitted for processing are revoked or other permissions cease to apply (e.g. if the purpose of processing this data has ceased to apply or it is not required for the purpose).

If the data are not deleted because they are required for other and legally permissible purposes, their processing will be limited to these purposes. That is, the data is blocked and not processed for other purposes. This applies, for example, to data that must be retained for reasons of commercial or tax law or whose storage is necessary for the assertion, exercise or defense of legal claims or for the protection of the rights of another natural or legal person.

Further information on the deletion of personal data can also be found in the individual data protection notices.

## Amendment and update of the privacy notice

Due to the further development of my website and offers on it or due to changed legal or regulatory requirements, it may become necessary to change this privacy policy. The current data protection notice can be accessed and printed out at any time on the website under the link.

## Rights of the data subjects

As a data subject, you have various rights under the GDPR:

- Pursuant to Art.15 GDPR** to request information about your personal data processed by me. In particular, you can request information about the processing purposes, the category of personal data, the categories of recipients to whom your data have been or will be disclosed, the planned storage period, the existence of a right to rectification, erasure, restriction of processing or opposition, the existence of a right of complaint, the origin of your data, if they were not collected by me, as well as the existence of automated decision-making, including profiling and, if applicable, meaningful information about their details.
- Pursuant to Art.16 GDPR**, you may immediately request the correction or completion of your personal data stored by me.
- Pursuant to Art.17 GDPR**, to request the deletion of your personal data stored by me, unless the processing is necessary for the exercise of the right to freedom of expression and information, for compliance with a legal obligation, for reasons of public interest or for the assertion, exercise or defense of legal claims.
- Pursuant to Art.18 GDPR**, to request the restriction of the processing of your personal data, insofar as the accuracy of the data is disputed by you, the processing is unlawful, but you object to its erasure, I no longer need the data, but you need it for the assertion, exercise or defense of legal claims or you have objected to the processing pursuant to Art.21 GDPR.
- Pursuant to Art.20 GDPR**, to receive your personal data that you have provided to me in a structured, common and machine-readable format or to request the transfer to another controller.
- **Pursuant to Art.7 para.3 GDPR** to revoke your consent once given to me at any time. This has the consequence that I may no longer continue the data processing based on this consent for the future.
- **Pursuant to Art.77 GDPR** to complain to a supervisory authority. As a rule, you can contact the supervisory authority of your usual place of residence or workplace for this purpose.

## External payment service providers

Visitors to this website can support the operator financially via payment service providers. The privacy policies of the respective providers apply:

- Open Collective: [Open Collective Privacy Policy](https://opencollective.com/privacypolicy)
- Bank transfer: in the case of a bank transfer, the first and last name, as well as the reason for the transfer, the date of the transaction, IBAN, BIC and the amount are usually transmitted.

## Address processing

All the contact information of me given on this website including any photos are expressly for information purposes only or to contact me. In particular, they may not be used for sending advertising, spam and the like. An advertising use of this data is therefore hereby contradicted. Should this information nevertheless be used for the aforementioned purposes, I reserve the right to take legal action.
