---
title: Datenschutzhinweise
date: 2020-09-01T00:00:00.000Z
lastmod: 2023-02-03T18:51:44.891Z
tags:
  - ""
categories:
  - ""
slug: privacy
---

## Einleitung

Wir haben uns bewusst gegen den Einsatz von Tracking-Cookies, Analyse-Werkzeugen oder andere direkte Einbindungen fremder Inhalte entschieden. Betreiber von verlinkten Webseiten bekommen erst Ihre Informationen, wenn du den Link anklickst. Dieses Vorgehen ist nicht dem »Mainstream« entsprechend und hat Nachteile für das Ranking bei Google und Co., aber das nehmen wir gerne in Kauf. Dies handhaben wir so, um die [informationelle Selbstbestimmung](https://de.wikipedia.org/wiki/Informationelle_Selbstbestimmung) zu fördern, genauer gesagt deine IT-Sicherheit nicht unnötig zu gefährden.

Die verwendeten Begriffe sind nicht geschlechtsspezifisch.

## Name und Kontaktdaten des für die Verarbeitung Verantwortlichen

<p class="bot">
kcuB leinaD<br/>
<span style="font-size: 10px;">neinareM nov gozreH</span><br/>
2 .rtsnehcriK<br/>
nralsE 39629
</p>

**E-Mail-Adresse:** <spawn class="bot">retsnom.leipsnellor@retsambew</spawn> <a href="https://rollenspiel.monster/.well-known/openpgpkey/hu/kd39y8fkyw5j8uubuicshffo9hhodk4j?l=webmaster"><i class="fa fa-gnupg" aria-hidden="true"></i>-Schlüssel</a><br/>
**Telefon:** <spawn class="bot">77736587 651 94+</spawn>

## Ansprechpartner für datenschutzrechtliche Fragen

Bei Fragen zum Datenschutz stehen wir unter <a class="bot">retsnom.leipsnellor@ztuhcsnetad</a> <a href="https://rollenspiel.monster/.well-known/openpgpkey/hu/i7jib9hs56n983nznn7ohu6yz49drzkb?l=datenschutz"><i class="fa fa-gnupg" aria-hidden="true"></i>-Schlüssel</a> oder unter der oben angegebenen postalischen Anschrift zur Verfügung.

## Bereitstellung des Onlineangebotes und Webhosting

### Hosting

Wir hosten die Inhalte unserer Website bei folgendem Anbieter:

#### Hetzner

Anbieter ist die Hetzner Online GmbH, Industriestr. 25, 91710 Gunzenhausen (nachfolgend Hetzner).

Details entnehmen Sie der Datenschutzerklärung von Hetzner: https://www.hetzner.com/de/rechtliches/datenschutz.

Die Verwendung von Hetzner erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Wir haben ein berechtigtes Interesse an einer möglichst zuverlässigen Darstellung unserer Website. Sofern eine entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschließlich auf Grundlage von Art.
6 Abs. 1 lit. a DSGVO und § 25 Abs. 1 TTDSG, soweit die Einwilligung die Speicherung von Cookies oder den Zugriff auf Informationen im Endgerät des Nutzers (z. B. Device-Fingerprinting) im Sinne des TTDSG umfasst. Die Einwilligung ist jederzeit widerrufbar.

##### Auftragsverarbeitung

Wir haben einen Vertrag über Auftragsverarbeitung (AVV) zur Nutzung des oben genannten Dienstes geschlossen. Hierbei handelt es sich um einen datenschutzrechtlich vorgeschriebenen Vertrag, der gewährleistet, dass dieser die personenbezogenen Daten unserer Websitebesucher nur nach unseren Weisungen und unter Einhaltung der DSGVO verarbeitet.

### Erhebung von Zugriffsdaten und Logfiles

Wenn du unsere Webseite besuchst, übermittelt dein Browser systembedingt gewisse Informationen an den Server, die dich wiederum identifizierbar machen könnten. So übermittelst du, genauer gesagt dein Webbrowser z. B. Informationen wie die von deinem Provider zugewiesene IP-Adresse.

Dieses systembedingte Verhalten deines Browsers kannst du grundsätzlich nicht verhindern und es bleibt dir deshalb auch nichts anderes übrig, als darauf zu vertrauen, dass wir mit den übermittelten Informationen sensibel umgehe. Weil uns – auch wenn es ein wenig abgedroschen klingen mag – der Schutz deiner Daten tatsächlich am Herzen liegt, werden wir die übermittelten Informationen bestmöglich deiner Interessen vertretend handhaben. Deswegen erheben, verarbeiten oder nutzen wir die übermittelten Informationen nicht zu Analyse-, Werbezwecken oder Ähnlichem. Vielmehr speichern wir keine Informationen über dich – der Webserver auf Basis von nginx ist so konfiguriert, dass er keine Logfiles schreibt:

```
## LOGS ##
access_log off;
error_log off;
```

### SSL- bzw. TLS-Verschlüsselung

Diese Seite nutzt aus Sicherheitsgründen und zum Schutz der Übertragung vertraulicher Inhalte, wie zum Beispiel Bestellungen oder Anfragen, die du an uns als Seitenbetreiber senden, eine SSL- bzw. TLS-Verschlüsselung. Eine verschlüsselte Verbindung erkennst du daran, dass die Adresszeile des Browsers von „http://“ auf „https://“ wechselt und an dem Schloss-Symbol in deiner Browserzeile.

Wenn die SSL- bzw. TLS-Verschlüsselung aktiviert ist, können die Daten, die du an uns übermittelst, nicht von Dritten mitgelesen werden.

### Einsatz von Cookies

Cookies sind Textdateien, die Daten von besuchten Websites oder Domains enthalten und von einem Browser auf dem Computer des Benutzers gespeichert werden. Ein Cookie dient in erster Linie dazu, die Informationen über einen Benutzer während oder nach seinem Besuch innerhalb eines Onlineangebotes zu speichern. Zu den gespeicherten Angaben können z. B. die Spracheinstellungen auf einer Webseite, der Loginstatus, ein Warenkorb oder die Stelle, an der ein Video geschaut wurde, gehören. Zu dem Begriff der Cookies zählen wir ferner andere Technologien, die die gleichen Funktionen wie Cookies erfüllen (z. B., wenn Angaben der Nutzer anhand pseudonymer Onlinekennzeichnungen gespeichert werden, auch als "Nutzer-IDs" bezeichnet)

Die durch die Cookies verarbeiteten Daten sind für die Bereitstellung unserer Services und somit zur Wahrung unserer berechtigten Interessen sowie der Dritter nach Art. 6 Abs. 1 S. 1 lit. f DSGVO erforderlich. Bei den gesetzten Cookies, welche z. B. für den Login notwendig sind, handelt es sich um persistente Cookies, die allerdings nicht mit Tracking-Cookies von Drittanbietern verwechselt werden dürfen, die einen Nutzer seitenübergreifend über das Internet verfolgen.

**Hinweis**: Die meisten Browser akzeptieren Cookies automatisch. Du kannst deinen Browser jedoch so konfigurieren, dass keine Cookies auf deinem Computer gespeichert werden oder stets ein Hinweis erscheint, bevor ein neuer Cookie angelegt wird. Die vollständige Deaktivierung von Cookies kann jedoch dazu führen, dass du nicht alle Funktionen unserer Webseite nutzen kannst.

### Plugins und Tools

#### hCaptcha

Wir nutzen hCaptcha (im Folgenden „hCaptcha“) auf dieser Website. Anbieter ist die Intuition Machines, Inc., 2211 Selig Drive, Los Angeles, CA 90026, USA (im Folgenden „IMI“).

Mit hCaptcha soll überprüft werden, ob die Dateneingabe auf dieser Website (z. B. in einem Kontaktformular) durch einen Menschen oder durch ein automatisiertes Programm erfolgt. Hierzu analysiert hCaptcha das Verhalten des Websitebesuchers anhand verschiedener Merkmale.

Diese Analyse beginnt automatisch, sobald der Websitebesucher eine Website mit aktiviertem hCaptcha betritt. Zur Analyse wertet hCaptcha verschiedene Informationen aus (z. B. IP-Adresse, Verweildauer des Websitebesuchers auf der Website oder vom Nutzer getätigte Mausbewegungen). Die bei der Analyse erfassten Daten werden an IMI weitergeleitet. Wird hCaptcha im „unsichtbaren Modus“ verwendet, laufen die Analysen vollständig im Hintergrund. Websitebesucher werden nicht darauf hingewiesen, dass eine Analyse stattfindet.

Die Speicherung und Analyse der Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse daran, seine Webangebote vor missbräuchlicher automatisierter Ausspähung und vor SPAM zu schützen. Sofern eine entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschließlich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO und § 25 Abs. 1 TTDSG, soweit die Einwilligung die Speicherung von Cookies oder den Zugriff auf Informationen im Endgerät des Nutzers (z. B. Device-Fingerprinting) im Sinne des TTDSG umfasst. Die Einwilligung ist jederzeit widerrufbar.

Die Datenverarbeitung wird auf Standardvertragsklauseln gestützt, die in den Datenverarbeitungszusatz zu den Allgemeinen Geschäftsbedingungen von IMI bzw. den Datenverarbeitungsverträgen enthalten sind.

Weitere Informationen zu hCaptcha entnehmen Sie den Datenschutzbestimmungen und Nutzungsbedingungen unter folgenden Links: [https://www.hcaptcha.com/privacy](https://www.hcaptcha.com/privacy) und [https://hcaptcha.com/terms](https://hcaptcha.com/terms).

Das Unternehmen verfügt über eine Zertifizierung nach dem „EU-US Data Privacy Framework“ (DPF). Der DPF ist ein Übereinkommen zwischen der Europäischen Union und den USA, der die Einhaltung europäischer Datenschutzstandards bei Datenverarbeitungen in den USA gewährleisten soll. Jedes nach dem DPF zertifizierte Unternehmen verpflichtet sich, diese Datenschutzstandards einzuhalten. Weitere Informationen hierzu erhalten Sie vom Anbieter unter folgendem Link: [https://www.dataprivacyframework.gov/list](https://www.dataprivacyframework.gov/list)

## Kontaktaufnahme

Bei der Kontaktaufnahme mit uns (z. B. per Kontaktformular, E-Mail, Telefon oder via soziale Medien) werden die Angaben der anfragenden Personen verarbeitet, soweit dies zur Beantwortung der Kontaktanfragen und etwaiger angefragter Maßnahmen erforderlich ist. Dabei ist die Angabe einer gültigen E-Mail-Adresse erforderlich, damit ich weiß, von wem die Anfrage stammt und um diese beantworten zu können. Weitere Angaben können freiwillig getätigt werden.

## Präsenzen in sozialen Netzwerken (Social Media)

Wir unterhalten Onlinepräsenzen innerhalb sozialer Netzwerke und verarbeiten in diesem Rahmen Daten der Nutzer, um mit den dort aktiven Nutzern zu kommunizieren oder um Informationen über uns anzubieten.

Wir weisen darauf hin, dass dabei Daten der Nutzer außerhalb des Raumes der Europäischen Union verarbeitet werden können. Hierdurch können sich für die Nutzer Risiken ergeben, weil so z. B. die Durchsetzung der Rechte der Nutzer erschwert werden könnte.

## Löschung von Daten

Die von uns verarbeiteten Daten werden nach Maßgabe der gesetzlichen Vorgaben gelöscht, sobald deren zur Verarbeitung erlaubten Einwilligungen widerrufen werden oder sonstige Erlaubnisse entfallen (z. B., wenn der Zweck der Verarbeitung dieser Daten entfallen ist oder sie für den Zweck nicht erforderlich sind).

Sofern die Daten nicht gelöscht werden, weil sie für andere und gesetzlich zulässige Zwecke erforderlich sind, wird deren Verarbeitung auf diese Zwecke beschränkt. D. h., die Daten werden gesperrt und nicht für andere Zwecke verarbeitet. Das gilt z. B. für Daten, die aus handels- oder steuerrechtlichen Gründen aufbewahrt werden müssen oder deren Speicherung zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen oder zum Schutz der Rechte einer anderen natürlichen oder juristischen Person erforderlich ist.

Weitere Hinweise zu der Löschung von personenbezogenen Daten können ferner im Rahmen der einzelnen Datenschutzhinweise erfolgen.

## Änderung und Aktualisierung des Datenschutzhinweises

Durch die Weiterentwicklung meiner Webseite und Angebote darüber oder aufgrund geänderter gesetzlicher beziehungsweise behördlicher Vorgaben kann es notwendig werden, diesen Datenschutzhinweis zu ändern. Die jeweils aktuelle Datenschutzhinweise kann jederzeit auf der Webseite unter dem Link von Ihnen abgerufen und ausgedruckt werden.

## Rechte der betroffenen Personen

Dir steht als Betroffene nach der DSGVO verschiedene Rechte zu:

- **Gemäß Art.15 DSGVO** Auskunft über deine von mir verarbeiteten personenbezogenen Daten zu verlangen. Insbesondere kannst du Auskunft über die Verarbeitungszwecke, die Kategorie der personenbezogenen Daten, die Kategorien von Empfängern, gegenüber denen deine Daten offengelegt wurden oder werden, die geplante Speicherdauer, das Bestehen eines Rechts auf Berichtigung, Löschung, Einschränkung der Verarbeitung oder Widerspruch, das Bestehen eines Beschwerderechts, die Herkunft deiner Daten, sofern diese nicht bei mir erhoben wurden sowie über das Bestehen einer automatisierten Entscheidungsfindung einschließlich Profiling und ggf. aussagekräftigen Informationen zu deren Einzelheiten verlangen.
- **Gemäß Art.16 DSGVO** unverzüglich die Berichtigung oder Vervollständigung deiner bei mir gespeicherten personenbezogenen Daten verlangen.
- **Gemäß Art.17 DSGVO** die Löschung deiner bei mir gespeicherten personenbezogenen Daten zu verlangen, soweit nicht die Verarbeitung zur Ausübung des Rechts auf freie Meinungsäußerung und Information, zur Erfüllung einer rechtlichen Verpflichtung, aus Gründen des öffentlichen Interesses oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen erforderlich ist.
- **Gemäß Art.18 DSGVO** die Einschränkung der Verarbeitung deiner personenbezogenen Daten zu verlangen, soweit die Richtigkeit der Daten von dir bestritten wird, die Verarbeitung unrechtmäßig ist, du aber deren Löschung ablehnst, ich die Daten nicht mehr benötige, du jedoch diese zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen benötigst oder du gemäß Art.21 DSGVO Widerspruch gegen die Verarbeitung eingelegt hast.
- **Gemäß Art.20 DSGVO** deine personenbezogenen Daten, die du mir bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten oder die Übermittlung an einen anderen Verantwortlichen zu verlangen.
- **Gemäß Art.7 Abs.3 DSGVO** deine einmal erteilte Einwilligung jederzeit gegenüber mir zu widerrufen. Dies hat zur Folge, dass ich die Datenverarbeitung, die auf dieser Einwilligung beruhte, für die Zukunft nicht mehr fortführen darf.
- **Gemäß Art.77 DSGVO** sich bei einer Aufsichtsbehörde zu beschweren. In der Regel kannst du dich hierfür an die Aufsichtsbehörde deines üblichen Aufenthaltsortes oder Arbeitsplatzes wenden.

## Externe Zahlungsdienstleister

Über Zahlungsdienstleister können Besucher dieser Webseite den Betreiber finanziell unterstützen. Es gelten die Datenschutzbestimmungen der jeweiligen Anbieter:

- Open Collective: [Open Collective Privacy Policy](https://opencollective.com/privacypolicy)
- Banküberweisung: bei einer Überweisung wird in der Regel Vor- und Zuname, sowie Verwendungszweck, Buchungsdatum, IBAN, BIC und Betrag übermittelt.

## Adressverarbeitung

Alle die auf dieser Webseite angegebenen Kontaktinformationen von mir inklusive etwaiger Fotos dienen ausdrücklich nur zu Informationszwecken bzw. zur Kontaktaufnahme. Sie dürfen insbesondere nicht für die Zusendung von Werbung, Spam und ähnliches genutzt werden. Einer werblichen Nutzung dieser Daten wird deshalb hiermit widersprochen. Sollten diese Informationen dennoch zu den vorstehend genannten Zwecken genutzt werden, behalte ich mir etwaige rechtliche Schritte vor.
