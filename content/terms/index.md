---
title: Nutzungsbedingungen
date: 2021-04-05T00:00:00.000Z
lastmod: 2024-08-16T17:22:45.937Z
tags:
  - ""
categories:
  - ""
slug: terms
---

## Präambel

Durch die Nutzung unserer Dienste erklären Sie sich mit den folgenden Bedingungen und Konditionen einverstanden.
<b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> behält sich das Recht vor, diese Bedingungen gelegentlich zu aktualisieren und zu ändern.

## Kurzversion

Zur besseren Lesbarkeit, ohne den juristischen Fachjargon, bieten wir im Folgenden eine Version an, die ein normaler Mensch verstehen kann.

<div class="alert alert-success">

Sie sind Eigentümer Ihrer Inhalte (aber wir ermutigen Sie, sie unter einer freien Lizenz zu veröffentlichen).

<b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> wird Ihre persönlichen Daten nicht verwerten, außer für interne Statistiken (anonymisiert) oder um Sie über eine wichtige Änderung unserer Dienste zu informieren.

<b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> wird Ihre persönlichen Daten nicht weitergeben oder weiterverkaufen (Ihre Privatsphäre ist - wirklich - wichtig für uns).

Die Verpflichtungen von <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> sind das Ergebnis eines Ansatzes, der von dem Wunsch eines freien und selbstbestimmten digitalen Leben inspiriert wurde. Zu finden in unserer Charta der freien, ethischen, dezentralen und solidarischen Dienste.

</div>

<div class="alert alert-warning">

**Das Gesetz ist das Gesetz:** Sie müssen das Gesetz befolgen (egal ob es gut oder dumm ist), oder Ihr Konto wird gelöscht.

**Wenn es kaputtgeht, sind wir nicht zur Reparatur verpflichtet:** <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> bietet diesen Service kostenlos und frei an. Wenn Sie Daten verlieren, durch Ihr oder unser Verschulden, tut es uns leid, aber das passiert. Wir werden alles, was wir können tun, um sie wiederzuerlangen, aber wir leiten daraus keine Verpflichtung ab. Mit anderen Worten: Vermeiden Sie es, sensible oder wichtige Daten auf <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> Dienste zu legen, da wir im Falle eines Verlustes keine Garantie für deren Wiederherstellung übernehmen können.

**Wenn Sie nicht zufrieden sind, können Sie woanders hingehen:** Wenn Ihnen der Service nicht zusagt, steht es Ihnen frei, woanders einen gleichwertigen (oder besseren) Service zu finden oder einen eigenen zu gründen.

**Jeglicher Missbrauch wird bestraft:** Wenn ein Benutzer den Dienst missbraucht, z. B. durch Monopolisierung gemeinsam genutzter Rechnerressourcen oder durch Veröffentlichung von Inhalten, die als irrelevant angesehen werden, können seine Inhalte oder sein Konto ohne Warnung oder Verhandlung gelöscht werden. <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> bleibt der alleinige Richter über diesen Begriff des "Missbrauchs", um allen seinen Benutzern den bestmöglichen Service zu bieten. Wenn Ihnen das antidemokratisch oder gegen die Meinungsfreiheit erscheint, möchten wir auf den vorigen Absatz verweisen.

**Nichts ist für die Ewigkeit:** Dienste können geschlossen werden (z. B. wegen fehlender Mittel zu ihrer Aufrechterhaltung), sie können Opfer von Eindringlingen werden (die "hundertprozentige Sicherheit" gibt es nicht). Wir empfehlen Ihnen daher, eine Kopie der für Sie wichtigen Daten aufzubewahren, da <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> nicht für das zeitlich unbegrenzte Hosting verantwortlich gemacht werden kann.

</div>

## Vollversion

### Bedingungen für den Service

- Die Nutzung unsere Dienste erfolgt auf eigene Gefahr. Der Dienst wird so bereitgestellt, wie er ist.
- Sie dürfen eine andere Website nicht so verändern, dass sie fälschlicherweise den Eindruck erweckt, sie sei mit diesem <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b>\-Dienst verbunden.
- Unsere Dienste stehen nur natürliche oder juristische Person zur Verfügung. Konten, die durch Robots oder andere automatisierte Methoden erstellt wurden, können ohne Vorankündigung gelöscht werden.
- Sie sind für die Sicherheit Ihres Kontos und Ihres Passworts verantwortlich. <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> kann und wird nicht für Verluste oder Schäden haften, die sich aus der Nichteinhaltung dieser Sicherheitsverpflichtung durch Sie ergeben.
- Sie sind für alle geposteten Inhalte und Aktivitäten, die unter Ihrem Konto stattfinden, verantwortlich.
- Sie dürfen den Dienst nicht für illegale oder nicht autorisierte Zwecke nutzen.
- Sie dürfen nicht gegen die Gesetze Ihres Landes verstoßen.
- Es ist Ihnen nicht gestattet, ein Konto aus dem genutzten Dienst zu verkaufen, zu tauschen, weiterzuverkaufen oder für einen nicht autorisierten kommerziellen Zweck zu nutzen.
- Sie verstehen und stimmen zu, dass <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> nicht für den Inhalt verantwortlich gemacht werden kann, der in unseren Diensten veröffentlicht wird.
- Sie verstehen, dass die Online-Stellung der Dienste und Ihrer Inhalte eine Übertragung (in klarer oder verschlüsselter Form, je nach Dienst) über verschiedene Netzwerke beinhaltet.
- Sie dürfen keine Viren oder sonstigen Code bösartiger Natur übertragen.
- <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> übernimmt keine Garantie dafür, dass
  - unsere Dienste Ihren speziellen Anforderungen entspricht,
  - unsere Dienste ununterbrochen oder fehlerfrei sein werden,
  - dass Fehler im Service korrigiert werden.
- Sie verstehen und stimmen zu, dass <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> nicht haftbar gemacht werden kann für direkte, indirekte, zufällige oder Folgeschäden, einschließlich Schäden für entgangenen Gewinn, Firmenwert, Zugang, Daten oder andere immaterielle Verluste (selbst wenn <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> auf die Möglichkeit solcher Schäden hingewiesen wurde), die aus:
  - die Nutzung oder Unmöglichkeit der Nutzung unserer Dienste
  - unbefugter Zugriff auf oder Veränderung der Datenübertragung;
  - die Aussagen oder Handlungen einer dritten Partei auf einem Dienst
  - Beendigung Ihres Kontos
  - jede andere Angelegenheit im Kontext der Dienste
- Das Versäumnis von <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b>, ein Recht oder eine Bestimmung der Servicebedingungen auszuüben oder durchzusetzen, stellt keinen Verzicht auf dieses Recht oder diese Bestimmung dar. Die Nutzungsbedingungen stellen die gesamte Vereinbarung zwischen Ihnen und <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> dar und regeln Ihre Nutzung des Dienstes und ersetzen alle vorherigen Vereinbarungen zwischen Ihnen und <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> (einschließlich früherer Versionen der Nutzungsbedingungen).
- Sie stimmen die [Charta](/charta/) gelesen zu haben und akzeptieren die daraus resultierenden Moderationsmaßnahmen

### Änderungen am Dienst

- <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> behält sich das Recht vor, einen Dienst jederzeit mit oder ohne Vorankündigung zu ändern oder vorübergehend oder dauerhaft zu unterbrechen.
- <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> haftet weder Ihnen noch Dritten gegenüber für eine Änderung, Aussetzung oder Unterbrechung eines Dienstes.

### Urheberrecht am Inhalt

- Es ist Ihnen nicht gestattet, Inhalte zu veröffentlichen, hochzuladen, zu bloggen, zu verteilen, zu senden oder zu verbreiten, die illegal, verleumderisch, belästigend, missbräuchlich, betrügerisch, verletzend, obszön oder anderweitig zu beanstanden sind.
- Sie dürfen keine Inhalte hochladen oder zur Verfügung stellen, die die Rechte anderer verletzen.
- Wir beanspruchen keine Rechte an Ihren Daten: Text, Bilder, Ton, Video oder sonstiges Material, das Sie von Ihrem Konto hochladen oder übertragen.
- Wir werden Ihre Inhalte für keinen anderen Zweck verwenden als für die Bereitstellung des Dienstes an Sie.

### Bearbeitung und gemeinsame Nutzung von Daten

- Die Dateien, die Sie mit dem Dienst erstellen, können - wenn Sie möchten - von Personen, die Sie kennen oder nicht, gelesen, kopiert, verwendet und weiterverteilt werden.
- Durch die Veröffentlichung Ihrer Daten erkennen Sie an und erklären sich damit einverstanden, dass jeder, der diese Website nutzt, sie ohne Einschränkungen einsehen kann.
- Der Dienst kann Ihnen aber auch ermöglichen, den Zugriff und die gemeinsame Arbeit an seinen Dokumenten eingeschränkt einem oder mehreren anderen Benutzern zu gestatten.
- <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> kann nicht für Probleme verantwortlich gemacht werden, die aus dem Austausch oder der Veröffentlichung von Daten zwischen Benutzern resultieren.

### Beendigung

- <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> hat nach eigenem Ermessen das Recht, Ihr Konto auszusetzen oder zu kündigen und jegliche aktuelle oder zukünftige Nutzung des Dienstes zu verweigern. Eine solche Beendigung des Dienstes hat die Deaktivierung des Zugangs zu Ihrem Konto und die Rückgabe aller Inhalte zur Folge.
- <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> behält sich das Recht vor, einen Dienst für jedermann aus beliebigen Gründen jederzeit zu verweigern.
- <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> behält sich außerdem das Recht vor, Ihr Konto zu kündigen, wenn Sie sich über einen Zeitraum von mehr als 6 Monaten nicht in Ihr Konto einloggen.
- Bevor <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> einen Dienst beendet, werden die jeweiligen Nutzer drei Monate im Voraus darauf aufmerksam gemacht.

### Persönliche Daten

- Um bestimmte <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> Dienste nutzen zu können, müssen Sie ein Konto anlegen. <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> benötigt bestimmte persönliche Informationen: eine gültige E-Mail-Adresse und ein Passwort, das dazu dient, Ihr Konto vor unberechtigtem Zugriff zu schützen. Die Felder "Nachname" und "Vorname" können für den ordnungsgemäßen Betrieb der Software erforderlich sein, müssen aber nicht Ihre wahre Identität preisgeben.
- Wie andere Online-Dienste auch, speichert <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> automatisch bestimmte Informationen über Ihre Nutzung des Dienstes, wie Kontoaktivitäten (z. B. genutzter Speicherplatz, Anzahl der Einträge, durchgeführte Aktionen), angezeigte oder angeklickte Daten (z. B. Links, Elemente der Benutzeroberfläche) und andere Informationen, die Sie identifizieren (z. B. Browsertyp, IP-Adresse, Datum und Uhrzeit des Zugriffs, verweisende URL). Wir verwenden diese Informationen intern, um die Benutzeroberfläche der <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> Dienste zu verbessern und um eine konsistente und zuverlässige Benutzererfahrung zu gewährleisten. Diese Daten werden nicht verkauft oder an Dritte weitergegeben.

### Änderungen an den Bedingungen

- Diese Nutzungsbedingungen können jederzeit und einseitig durch <b class="firstpart">Rollenspiel</b><b class="lastpart">Monster</b> geändert werden.
- Alle Änderungen werden mindestens 14 Tage vor Inkrafttreten über den Admin-Account bekannt gegeben.
