RollenspielMonster Homepage
============
![Framework](https://img.shields.io/badge/Framework-hugo-pink) ![Lizenz](https://img.shields.io/badge/Lizenz-CC--BY--NC--SA--4.0-orange) [![SourceCode](https://img.shields.io/badge/SoruceCode-codeberg.org-green)](https://codeberg.org/Tealk/pages-source)

# Allgemein
Dies ist das Repository unsere Projektseite welche mit Hugo erstellt wurde.

## Funktionsumfang
Die Einstiegsseite zeigt eine übersicht all unserer Dienste

### News
In diesem Bereich finden sich alle Informationen über Änderungen der bestehenen Services oder Neuzugänge.

### Charta
Eine art verhaltenskodesx zu unseren Diensten.

### Spenden
Darunter finden sich ein paar möglichkeiten wie ihr dem Projekt helfen könnt.

### Rechtliches
Unter Nutzungsbedingungen, Impressum und Datenschutzerklärung finden sich die entsprechenden Texte für alle unsere Services. 

## Bilder
Die Hintergrundbilder sind von folgenden Künstlern erstellt worden:
* https://www.artstation.com/aaronflorento
* https://www.artstation.com/jobpmenting