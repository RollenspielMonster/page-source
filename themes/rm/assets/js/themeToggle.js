document.addEventListener("DOMContentLoaded", function () {
  var lightLink = document.getElementById("theme_light");
  var darkLink = document.getElementById("theme_dark");
  var autoLink = document.getElementById("theme_auto");

  lightLink.addEventListener("click", set_light);
  darkLink.addEventListener("click", set_dark);
  autoLink.addEventListener("click", set_auto);
});

if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
  document.documentElement.setAttribute("data-bs-theme", "dark");
  document.documentElement.setAttribute("bs-theme", "auto");
}

function set_light() {
  document.documentElement.setAttribute("data-bs-theme", "light");
}

function set_dark() {
  document.documentElement.setAttribute("data-bs-theme", "dark");
}

function set_auto() {
  if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
    document.documentElement.setAttribute("data-bs-theme", "dark");
  } else {
    document.documentElement.removeAttribute("data-bs-theme");
    document.documentElement.setAttribute("bs-theme", "auto");
  }
}
